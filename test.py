from run import (
    get_by_class,
    is_wanted,
    get_attrs,
    get_id,
    add_newly,
    update2disk,
)

import json

from unittest import TestCase


class Test_get_by_class(TestCase):

    def test(self):
        page = {}
        with open('page.json', 'r') as f:
            page = json.load(f)

        klass = 'd-table table-fixed col-12 width-full py-4 border-bottom color-border-muted'
        res = get_by_class(page['root'], klass)
        print(len(get_by_class(page['root'], klass)))
        #with open('result.json', 'w') as f:
        #    json.dump(res, f, indent=2)

        ret = is_wanted(res[27], locations=['shenzhen'])
        print(ret)
        attrs = get_attrs(res[28], t='attributes')
        print(get_id(res[27]))

    def test_add(self):
        db = []
        newly = [{"id": "1", "email": "1@163.com"}]

        add_newly(db, newly, user="2")
        self.assertEqual(len(db), 1)
        self.assertListEqual(db[0]["following"], ["2"])

        add_newly(db, newly, user="3")
        self.assertEqual(len(db), 1)
        self.assertListEqual(db[0]["following"], ["2", "3"])

        newly = [{"id": "2", "email": "2@163.com"},
                 {"id": "3", "email": "3@163.com"}]
        add_newly(db, newly, user="3")
        self.assertEqual(len(db), 3)
        self.assertListEqual(db[0]["following"], ["2", "3"])
        print(db)


    def test_update(self):
        with open("/tmp/1.json", 'w') as f:
            json.dump([], f)

        newly = [{"id": "2", "email": "2@163.com"},
                 {"id": "3", "email": "3@163.com"}]

        update2disk(newly, "lin", "/tmp/1.json")

        with open("/tmp/1.json", 'r') as f:
            res = json.load(f)
            self.assertEqual(len(res), 2)
