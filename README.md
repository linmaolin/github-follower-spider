## Introduction

This is a script I'm using to fetch some email from github, just for recruitment. As we need more talent partners. The script is based on Python3, and may work on Python2 too.


## How to run it?

### On Windows

1. Change Chrome start cmd

Find the "chrome" shortcut on the desktop, change the "TARGET" parts to add the port. The result is shown as below.

```
"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --remote-debugging-port=9222
```

Then run the Chrome and log into you github account. If Chrome is running already, you need to reopen it.


2. Find the user ID

The user is the one whose followers you want to fetch, of course we can only fetch the public personal information. I only fetch the e-mail by now.


3. Run the script


You need the pip3 to install pychrome package.

```
pip3 install pychrome
```

As the precise total number is not fetched automatically, you need to specific it personally.

```
python3 run.py user-id follower-number
```

All the result will be saved in `db.json`, a json list looks like this.

```
[
  {
    "id": "xxx",
    "email": "xxx@foxmail.com",
    "time": "2022-03-13 15:00:40",
    "following": [
      "yyy"
    ]
  },
```
